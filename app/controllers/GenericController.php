<?php
class GenericController extends BaseController
{
    public function Login()
    {
        if (Request::isMethod('post')) {

            if (Input::has('email') && Input::has('password')) {
                $result = DB::selectOne('select * from lrv_tblUsers where email = ?',array(Input::get('email')));
                if(Hash::check(Input::get('password'),$result->password)){
                    //Session::put('name', Input::get('username'));
                    Session::put('Auth', 1);
                    Session::put('Name', $result->username);
                    return 1;
                }
                else{
                    return 0;
                }
            }
            return 0;
        } else {
            return View::make('login');
        }
    }

    public function Home()
    {
        if (Session::has('Auth')) {

            return View::make('home')->with(array('welcome_msg' => 'Hello, ' . Session::get('Name')));
        } else {
            return Redirect::route('login');
        }
    }

    public function Register()
    {
        if (Request::isMethod('post')) {
            if (Input::has('username') && Input::has('password') && Input::has('email')) {
                $username = Input::get('username');
                //$password = password_hash(Input::get('password'), PASSWORD_BCRYPT);
                $password = Hash::make(Input::get('password'));
                $email = Input::get('email');
                $result = DB::insert('insert into lrv_tblUsers values (default,?,?,?)', array($username, $password, $email));
                if ($result) {
                    return 1;
                } else {
                    return 0;
                }
            }
            return 0;
        } else {
            return View::make('register');
        }
    }

    public function Logout()
    {
        Session::forget('Name');
        Session::forget('Auth');
        return Redirect::route('/');
    }
    public function Test(){
        Mail::send('emails.test', array('firstname'=>'Umayr'), function($message){
            $message->to('ckcpark@gmail.com','Sajjad Wasim')->subject('Hello, mofo!');
        });

        return 0;
    }
}