<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Untitled Document</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
</head>
<body>
<div style="font-family: 'Open Sans', sans-serif;margin: 0;padding: 0;">
    <table style="background: #137bee; padding: 0; margin:0 ">
        <tr>
            <td>
                <div style="width:330px; padding-left:20px; text-align:justify; color:#FFF; font-size:12px;padding-top: 15px;">
                    <h2 style="font-weight:normal; text-transform: uppercase;margin: 0;padding: 0">Thank you for</h2>

                    <h1 style="text-transform: uppercase;margin: 0;padding: 0">Your participation</h1>

                    <p style="margin: 0;padding: 17px 0 0 0;">
                        As the lucky selected few, Dawlance is pleased to offer you a discount of PKR 1500 on its top of
                        the
                        line
                        Microwave Oven Models DW-112C (20 litres baking) and DW-115C (25 litres baking).
                        <br/>
                        In order to avail this opportunity and to learn more about how to avail this offer, kindly reply
                        to this
                        email
                        with your contact details and one of our sales representative will be in touch with you shortly.
                        <br/>
                        Upon availing this offer, selected few will stand a chance to invite a cooking expert to their
                        home and
                        have her
                        cook a dish of their choice from our cookbook.
                        <br/>
                        Hurry! This is a limited time offer till April 30th, 2014.</p>
                    <br/>
                    <a href="http://www.dawlance.com.pk" style="color: inherit;">www.dawlance.com.pk</a>
                    <a href="http://www.facebook.com/dawlancepakistan/" style="color: inherit;">facebook.com/dawlancepakistan</a>

                    <div
                        style="background-color:#811942; width: inherit;height: 32px; line-height: 32px; margin-top: 20px; text-align: center;">
                        <strong>Dawlance</strong> and <strong>You</strong> Making Pakistan <strong>Proud</strong></div>

                </div>
            </td>
            <td>
                <img alt="" src="http://fc09.deviantart.net/fs70/f/2014/107/6/e/dawlance1_by_umayrr-d7etd8b.jpg"/>
            </td>
        </tr>
    </table>

</div>
</body>
</html>