<!doctype html>
<html lang="en" ng-app="ng-laravel">
<head>
    <meta charset="UTF-8">
    <title>LOLOL</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/app.css"/>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/angular.min.js"></script>
</head>
<body>
<div class="container" ng-controller="registerCtrl">
    <form class="form-horizontal login register text-center">
        <div class="alert" ng-class="result.css" ng-show="result.css"><p><strong>{{result.msg.prefix}}</strong>
                {{result.msg.text}}</p></div>
        <input type="text" class="form-control has-success" placeholder="Full Name" ng-model="register.username"/>
        <input type="password" class="form-control" placeholder="Password" ng-model="register.password"/>
        <input type="email" class="form-control" placeholder="Email address" ng-model="register.email"/>
        <input type="button" value="Sign up!" ng-click="doRegister();" class="btn btn-primary btn-block"/>
    </form>
</div>
<script>

    var app = angular.module('ng-laravel', []);
    app.controller('registerCtrl', function ($scope, $http) {
        var httpConfig = {headers: { 'Content-Type': 'application/x-www-form-urlencoded' }};
        $scope.result = { css: null, msg: { prefix: '', text: ''} };
        $scope.register = {};
        $scope.doRegister = function () {
            $http.post('register', $.param($scope.register), httpConfig).success(function (data) {
                console.log(data);
                if (data == '1') {
                    $scope.result.css = 'alert-success';
                    $scope.result.msg.prefix = 'Woho!';
                    $scope.result.msg.text = 'You have been successfully registered.';
                }
                else {
                    $scope.result.css = 'alert-danger';
                    $scope.result.msg.prefix = 'Oh, snap!';
                    $scope.result.msg.text = 'Something went wrong.';
                }
            });
        }
    });
</script>
</body>
</html>
