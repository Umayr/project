<!doctype html>
<html lang="en" ng-app="ng-laravel">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/app.css"/>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/angular.min.js"></script>
</head>
<body>
<div class="container" ng-controller="loginCtrl">
    <form class="form-horizontal login text-center">
        <div class="alert alert-danger" ng-show="isInvalid"><p><strong>Ouch!</strong> Invalid username or password.</p>
        </div>
        <input type="email" class="form-control" placeholder="Email Address" ng-model="login.email"/>
        <input type="password" class="form-control" placeholder="Password" ng-model="login.password"/>
        <input type="button" value="Login" ng-click="doLogin();" class="btn btn-primary btn-block"/>
        <a href="register">Not a user? Sign up!</a>
    </form>
</div>
<script>
    var app = angular.module('ng-laravel', []);
    app.controller('loginCtrl', function ($scope, $http) {
        //var httpConfig = {headers: { 'Content-Type': 'application/x-www-form-urlencoded' }};

        $scope.login = {};
        $scope.isInvalid = false;
        $scope.doLogin = function () {
            $http.post('login', $scope.login).success(function (data) {
                console.log(data);
                if (data == '0') {
                    $scope.isInvalid = true;
                }
                else {
                    window.location = "home";
                }
            });
        }
    });
</script>
</body>
</html>
