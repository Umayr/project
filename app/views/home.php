<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/app.css"/>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/angular.min.js"></script>
</head>
<body>
<div class="container">
    <span class="spacer"></span>
    <a href="<?php echo URL::to('logout'); ?>">Logout</a>

    <div class="jumbotron">
        <h1><?php echo $welcome_msg; ?></h1>
    </div>
</div>

</body>
</html>