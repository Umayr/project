<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => '/', 'uses' => 'HomeController@init'));

Route::get('logout', array('as' => 'logout', 'uses' => 'GenericController@Logout'));

Route::get('login', array('as' => 'login', 'uses' => 'GenericController@Login'));
Route::post('login', 'GenericController@Login');

Route::get('home', array('as' => 'home', 'uses' => 'GenericController@Home'));

Route::get('register', array('as' => 'register', 'uses' => 'GenericController@Register'));
Route::post('register', 'GenericController@Register');


Route::get('test', 'GenericController@Test');